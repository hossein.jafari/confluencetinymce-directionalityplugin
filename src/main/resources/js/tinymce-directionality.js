define('com/sg/plugins', ['ajs', 'jquery'], function (AJS, $){
    "use strict";

    return {
        init : function(ed) {
            var tinymce = require('tinymce');
           
            const getAlignByDir = function(dir)
            {
                return (dir == 'rtl'? 'right' : 'left')
            }
            const setDirStyle = function(dom, element, dir)
            {
                dom.setAttrib(element, "dir", dir);
                dom.setStyle(element, 'direction', dir);
                dom.setStyle(element, 'text-align', getAlignByDir(dir))
                // Handel tables 
                if (element.localName == 'table' && 
                    element.className.includes('confluenceTable')) {
                    dom.setStyle(element, 'margin-'+ (dir == 'rtl'? 'left' : 'right') , 'auto')
                }
                
            }
           
            const isTaskListRoot = function(element) {
                if (element && element.localName == 'ul' && 
                    element.className.includes('inline-task-list')) {
                    return true;
                }
                return false;
            }

            const isTaskListItem = function(element) {
                if (element && element.localName == 'li' && 
                    element.attributes["data-inline-task-id"]) {
                    return true;
                }
                return false;
            }

            const getTaskListItemRoot = function (element)
            {
                for (; element; element = element.parentNode)
                {
                    if (isTaskListItem(element)) return element
                }
            }
            const getTaskListRoot = function (element)
            {
                //e.dom.getParent(e.selection.getStart(), "UL,OL")
                for (; element; element = element.parentNode)
                {
                    if (isTaskListRoot(element)) return element
                }
                return null;
            }
            const getNonBookmarkSibling = function(element)
            {
                var sibling = element.previousElementSibling
                while(isBookmark(sibling))
                {
                    sibling = sibling.previousElementSibling
                }
                return sibling
            }
            const hasSibling = function(block, pClass)
            {
                var sibling = getNonBookmarkSibling(block)
                return isHiddenDirp(sibling, pClass)
                
            }
            const removeSibling = function(editor, block, pClass)
            {
                if (hasSibling(block, pClass)) {
                    var bookmark = editor.selection.getBookmark();
                    editor.dom.remove(block.previousElementSibling, true)
                    editor.selection.moveToBookmark(bookmark);
                    //editor.selection.setRng(normalizeRange(editor.selection.getRng()))
                    //editor.focus()
                }
            }
            const addSibling = function (editor, block, dir, pClass)
            {
                if (isHiddenDirp(block, pClass))
                {
                    setDirStyle(editor.dom, block, dir)
                }
                else if (hasSibling(block, pClass)) {
                    setDirStyle(editor.dom, getNonBookmarkSibling(block), dir)
                }                  
                else {
                    var bookmark = addBookmark(editor)
                    var sibling = editor.dom.create('p', 
                    {'class' : pClass, 
                     'style' : 'margin:0; direction:'+ dir +'; text-align:'+ getAlignByDir(dir) 
                    })
                    block.insertAdjacentElement('beforebegin', sibling)
                    editor.selection.moveToBookmark(bookmark)
                }
            }
            
            var addBookmark = function(editor)
            {
                var bookmark = editor.selection.getBookmark();
                // var bookmarkSpan = editor.dom.$('#'+bookmark.id +'_start')[0]
                // var bookmarkParent =  bookmarkSpan.parentElement
                // //var lastChildNode = bookmarkParent.childNodes[bookmarkParent.childNodes.length -1]
                // bookmarkParent.appendChild(bookmarkSpan)
                return bookmark
            }

            const wrapChildren = function (block)
            {
                $(block).wrapInner('<span class="placeholder-inline-tasks"></span>')
            }   
           
            const isBookmark = function(element)
            {
                return (element && element.localName == 'span' && element.dataset.mceType == "bookmark")
            }
            const shouldWrapTaskListItem = function(li)
            {
                if (li && li.children.length == 0 ) return true
                for (var i =0; i < li.children.length; i++)
                {
                    var child = li.children[i]
                    if (!(child.localName == 'span' && !isBookmark(child)))
                    {
                        return true
                    }
                }
                return false

            }
            var isHiddenDirp = function(element, pClass)
            {
                return (element &&
                        element.localName == 'p' &&
                        element.className.includes(pClass))
            }
            const wrapTaskListItems = function (tasks, editor, dir)
            {   
                // wrap every li content so we can set style to containing p
                tinymce.each(tasks, function (li) {
                    if (isHiddenDirp(li))
                    {

                    }
                    // Wrap li content with p
                    else if (shouldWrapTaskListItem(li)){
                        var bookmark = editor.selection.getBookmark()
                        wrapChildren(li);
                        editor.selection.moveToBookmark(bookmark)
                    }   
                    // Although it will not be saved, Set style for li as well 
                    // for proper display in edit mode
                    setDirStyle(editor.dom,li,dir)                 
                    setDirStyle(editor.dom,li.children[0],dir)
                    //setDirClass(dom,li.children[0],dir)
                })
               
            }
                  
            const prepareTaskListForDirectionality = function(editor, root, tasks, dir)
            {
                addSibling(editor, root, dir, "task-list-hidden-dir-p")
                wrapTaskListItems(tasks, editor, dir)
            }
            const setDirection = function (editor, dir) {
                //debugger;
                
                const dom = editor.dom;
                const blocks = editor.selection.getSelectedBlocks();                
                if (blocks.length) {
                    //debugger;
                    
                    var taskLists =[]
                    var macros =[]
                    tinymce.each(blocks, function(block) {
                    var ul = getTaskListRoot(block)
                    if (ul) 
                    {
                        // If this is a subtext, get the parent
                        ul = getTaskListRoot(ul.parentNode) || ul
                        if (!taskLists.includes(ul)) {
                            taskLists.push(ul)
                        }
                    }
                    else if (block.attributes['data-macro-name']) {
                        if (!macros.includes(block)) {
                            macros.push(block)
                        }
                    }
                    else
                    {
                        setDirStyle(dom, block, dir); 
                    }
                                               
                    });
                    tinymce.each(taskLists, function(ul) { 
                        prepareTaskListForDirectionality(editor, ul, ul.children, dir)
                    })
                    tinymce.each(macros, function(macro) {
                        addSibling(editor, macro, dir, "hidden-dir-p")
                    })
                    editor.nodeChanged()
                    setButtonsByDir(dir)
                }
                editor.focus();
                
              }

            var doesContainTaskList = function(element)
            {
                return element.querySelector('ul.inline-task-list') != null
            }
            var getCurrDir = function (element, dom)
            {
                var curDir = dom.getAttrib(element, 'dir') ||
                            dom.getStyle(element, 'direction', true) ||
                            'ltr'
                return curDir
            }
            var setButtonsByDir = function(dir)
            {
                 // setting the direction button active 
                 $("#rtl-button a").removeClass('active');
                 $("#ltr-button a").removeClass('active');

                if(dir == 'rtl') {
                    $("#rtl-button a").addClass('active');
                } else if(dir == 'ltr') {
                    $("#ltr-button a").addClass('active');
                } 
                   
            }
            var setToolbarButtons = function (editor, element) {
                var dom = editor.dom;
                var curDir;
                const blocks = editor.selection.getSelectedBlocks();
                if (blocks.length) {
                    curDir = getCurrDir(blocks[0], dom)
                }
                setButtonsByDir(curDir)
                
            }
            ed.onNodeChange.add(function(editor,controlManager,element) {
                setToolbarButtons(editor, element)
            })
            ed.addCommand('mceDirectionLTR', function () {
                setDirection(ed, 'ltr');
             });
            
            ed.addCommand('mceDirectionRTL', function () {
                setDirection(ed, 'rtl');
              });
              
            ed.on('BeforeExecCommand', function (e) {
                var cmd = e.command//.toLowerCase();
                
                if (cmd === 'InsertInlineTaskList') {
                   
                    var editor = e.target
                    var selected = editor.selection.getNode()
                    var blocks = editor.selection.getSelectedBlocks(); 
                    var ul = getTaskListRoot(selected)
                    //var li = getTaskListItemRoot(selected)
                    // if (ul) {
                    //     var lis = []
                    //     tinymce.each(blocks, function(block) {
                    //         var li = getTaskListItemRoot(block)
                    //         if (li && !lis.includes(li))
                    //         {
                    //             lis.push(li)
                    //         }
                    //     })
                    //     // If entire list is removed
                    //     if (ul.children.length == lis.length) {
                    //         console.log('kuku')
                    //         //removeSibling(editor, ul, "task-list-hidden-dir-p")
                    //     }
                    
                    // } 
                    if (!ul)
                    {     
                        if (blocks.length >0 && !doesContainTaskList(selected))
                        {
                            //removeHiddenDirParagraphsFromSelection(selected, blocks)
                            prepareTaskListForDirectionality(editor,  blocks[0], blocks, getCurrDir(blocks[0], editor.dom))          
                        }
                        
                    }
                }
            })

            // Register button in a new group
            ed.addButton('ltr-button', {
                //default value of "toolbar" is "toolbar-primary"
                title: '¶',
                role: 'switch',
                tooltip: AJS.I18n.getText("com.example.plugins.confluence.tinymce-directionality.ltr.button.tooltip"),
                cmd: "mceDirectionLTR",
                className: "directionality",
                //icon: "aui-icon aui-icon-small aui-iconfont-arrow-right",
                //text: "kuku",//'¶',
                locationGroup: "rte-toolbar-group-justification",
                weight: 0
                //stateSelector: generateSelector('ltr')
            });
            
             ed.addButton('rtl-button', {
                //default value of "toolbar" is "toolbar-primary"
                title: '¶',
                tooltip: AJS.I18n.getText("com.example.plugins.confluence.tinymce-directionality.rtl.button.tooltip"),
                cmd: "mceDirectionRTL",
                className: "directionality",
                //icon: "aui-icon aui-icon-small aui-iconfont-arrow-left",
                locationGroup: "rte-toolbar-group-justification",
                weight: 0
                //stateSelector: generateSelector('rtl')
            });
            

            // Register the correct event, according to editor version
            if (ed.onInit) {
                ed.onInit.add(function(editor) {
                editor.dom.loadCSS(editor.documentBaseURI.getURI() +'/download/resources/com.sg.plugins.confluence.tinymce-directionality:tinymce-directionality-resources/tinymce-directionality.css');
                });
             }
             else if (ed.on) 
             {
                ed.on('init', function(e){  
                    e.target.dom.loadCSS('../../download/resources/com.sg.plugins.confluence.tinymce-directionality:tinymce-directionality-resources/tinymce-directionality.css');
                });
             }
            

        },  
        getInfo : function() {
            return {
                longname : 'Directionality',
                author : 'Shelly Goldblit',
                authorurl : 'http://www.sg.com',
                version : tinymce.majorVersion + "." + tinymce.minorVersion
            };
        }
    }
});

require('confluence/module-exporter')
    .safeRequire('com/sg/plugins', function(DirectionalityPlugin) {
        var tinymce = require('tinymce');
        console.log('Click here to debug tinymce-directionality plugin')
        tinymce.create('tinymce.plugins.DirectionalityPlugin', DirectionalityPlugin);

        // Register plugin
        tinymce.PluginManager.add('directionality', tinymce.plugins.DirectionalityPlugin);

        require('confluence-editor/loader/tinymce-bootstrap').addTinyMcePluginInit(function(settings) {
            settings.plugins += ",directionality";
            //Confluence.Templates.KeyboardShortcutsDialog.Autoformat.inlineTaskListDescription = function(opt_data, opt_ignored) {
            //    return '<ul class="inline-task-list" dir=ltr><li dir=ltr><p dir=ltr>task</p></li></ul>';
            //  };
            //settings.extended_valid_elements += ',li[*]'
            //settings.valid_children = '-li[strong|a|em,#text|time],+li[p,ul]'

        });
    });